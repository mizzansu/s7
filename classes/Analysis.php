<?php

/**
 * Created by PhpStorm.
 * User: pavelmyznikov
 * Date: 21.05.17
 * Time: 1:26
 */
class Analysis
{

    private $dbConnection;

    private $flights = [];

    private $commonDelta;

    public function __construct($period, $dbConnection)
    {
        $this->dbConnection = $dbConnection;


        $results = $this->dbConnection->query('SELECT * FROM analysis'.$period);
        if (!$results) {
            $this->flights = null;
            return false;
        }
        while ($row = $results->fetchArray()) {
            $this->flights[] = $row;
        }

    }

    public function getFlights()
    {

        $output = [];
        foreach ($this->flights as $flight) {

            $line = [
                'number' => $flight['flight_number'],
                'aircraft' => $flight['aircraft'],
                'date_plan' => $flight['date_plan'],
                'date_fact' => $flight['date_fact']
            ];

            $rows=0;

            if (intval($flight['econom_verdict']) == 0){
                $rows++;
                $line['Econom'] = [
                    'amount_passengers' => $flight['econom_amount_passengers'],
                    'amount_sets' => $flight['econom_sets'],
                    'set_to_be' => $flight['econom_set_to_be'],
                    'set_fact' => $flight['econom_set_fact'],
                    'set_delta' => $flight['econom_set_delta'],
                    'verdict' => $flight['econom_verdict'],
                    'errors' => explode(',', $flight['econom_errors'])
                ];
            }

            if (intval($flight['business_verdict']) == 0){
                $rows++;
                $line['Business'] = [
                    'amount_passengers' => $flight['business_amount_passengers'],
                    'amount_sets' => $flight['business_sets'],
                    'set_to_be' => $flight['business_set_to_be'],
                    'set_fact' => $flight['business_set_fact'],
                    'set_delta' => $flight['business_set_delta'],
                    'dishes_to_be' => explode(',', $flight['business_dishes_to_be']),
                    'dishes_fact' => explode(',', $flight['business_dishes_fact']),
                    'dishes_to_be_sum' => $flight['business_dishes_to_be_sum'],
                    'dishes_fact_sum' => $flight['business_dishes_fact_sum'],
                    'dishes_delta' => $flight['business_dishes_delta'],
                    'verdict' => $flight['business_verdict'],
                    'errors' => explode(',', $flight['business_errors'])
                ];
            }


            if (intval($flight['crew_verdict']) == 0){
                $rows++;
                $line['Crew'] = [
                    'amount_people' => $flight['crew_amount_passengers'],
                    'amount_sets' => $flight['crew_sets'],
                    'set_to_be' => $flight['crew_set_to_be'],
                    'set_fact' => $flight['crew_set_fact'],
                    'set_delta' => $flight['crew_set_delta'],
                    'dishes_to_be' => explode(',', $flight['crew_dishes_to_be']),
                    'dishes_fact' => explode(',', $flight['crew_dishes_fact']),
                    'dishes_to_be_sum' => $flight['crew_dishes_to_be_sum'],
                    'dishes_fact_sum' => $flight['crew_dishes_fact_sum'],
                    'dishes_delta' => $flight['crew_dishes_delta'],
                    'verdict' => $flight['crew_verdict'],
                    'errors' => explode(',', $flight['crew_errors'])
                ];
            }
            $line['rows'] = $rows;
            $output[] = $line;
        }

        $this->commonDelta = array_reduce($output, function($carry, $item){
            $delta=0;
            if (isset($item['Econom'])) {
                $delta += $item['Econom']['set_delta'] * $item['Econom']['amount_sets'];
            }
            if (isset($item['Business'])) {
                $delta += $item['Business']['set_delta'] * $item['Business']['amount_sets'];
                $delta += $item['Business']['dishes_delta'] * $item['Business']['amount_passengers'];
            }
            if (isset($item['Crew'])) {
                $delta += $item['Crew']['set_delta'] * $item['Crew']['amount_sets'];
                $delta += $item['Crew']['dishes_delta'] * $item['Crew']['amount_people'];
            }

            return $delta + $carry;
        }, 0);

        return $output;
    }

    public function getDelta(){
        return $this->commonDelta;
    }
}