<?php
/**
 * Created by PhpStorm.
 * User: pavelmyznikov
 * Date: 20.05.17
 * Time: 17:05
 */
?>


<?php include('../template/header.php'); ?>

<?php
    include('../classes/Analysis.php');

    $period = str_replace('-','', $_GET['date_from']).'_'.str_replace('-','', $_GET['date_to']);

    $analysis = new Analysis($period, $dbConnection);

?>




<div class="starter-template">
    <h1 style="color: #666">Проверка периода <?=$_GET['date_from']?> - <?=$_GET['date_to']?></h1>


    <?php    $flights = $analysis->getFlights();?>

    <?php if ($flights == null){?>
        <p>
            Для данного периода данные не загружены.
        </p>
        <?php include('../template/footer.php')?>
        <?php die();?>
    <?}?>


    <p>
        <span>Количество рейсов с ошибками: <span class="badge alert-danger"><?=count($flights)?></span></span>
        Суммарная дельта: <span class="badge alert-danger"><?=number_format($analysis->getDelta()/2, 2, ',', ' ');?></span>

    </p>

    <br />
    <?php    $flights = $analysis->getFlights(); ?>
    <?php
    $y_errors = [];
    $c_errors = [];
    $k_errors = [];
    foreach ($flights as $t){
        foreach ($t as $k => $f) {
            if (!in_array($k, ['Econom', 'Crew', 'Business']))
                continue;
//                var_dump($f['errors']);
            if ($k == 'Econom'){
//                var_dump($y_errors);
//                var_dump($f['errors']);
//                $y_errors += $f['errors'];
                $y_errors = array_merge($y_errors, $f['errors']);
//                var_dump($y_errors);
//                exit();
            }
            if ($k == 'Crew'){
//                $k_errors += $f['errors'];
                $k_errors = array_merge($k_errors, $f['errors']);
            }
            if ($k == 'Business'){
//                $c_errors += $f['errors'];
                $c_errors = array_merge($c_errors, $f['errors']);
            }
        }
    }
    $e_types = [
        'Количество комплектов эконома больше положенного',
        'Комплект эконома не верен',
        'Комплект бизнес-класса не верен',
        'Комплект экипажа не верен',
//            'Стоимость блюд бизнес-класса некорректна',
        'Неправильный набор блюд для бизнес-класса'
    ];
    $e_types2 = [
        'Количество комплектов не верно',
        'Комплект не верен',
//        'Стоимость блюд некорректна',
        'Неправильный набор блюд'
    ];
    $eet = ['Неправильный', 'Количество', 'Комплект', 'Стоимость'];
    //        echo "<pre>";
    //        var_dump($k_errors, $c_errors, $y_errors);
    $c_err_num = array_count_values($c_errors);
    $y_err_num = array_count_values($y_errors);
    $k_err_num = array_count_values($k_errors);
    //        var_dump($c_err_num);
    //        echo "</pre>";
    echo "<table class='table table-bordered table-condensed'><tbody><tr><th></th>";
    foreach ($e_types2 as $e_type){
        echo "<th>" .$e_type . "</th>";
    }
    echo "</tr><tr><th>Бизнес</th>";
    foreach($e_types as $e_type){
        if (in_array($e_type, ['Комплект эконома не верен','Комплект экипажа не верен']))
            continue;
        echo "<td>" . ($c_err_num[$e_type] ?: 0) . "</td>";
    }
    echo "</tr><tr><th>Эконом</th>";
    foreach($e_types as $e_type){
        if (in_array($e_type, ['Комплект бизнес-класса не верен','Комплект экипажа не верен']))
            continue;
        echo "<td>" . ($y_err_num[$e_type] ?: 0) . "</td>";
    }
    echo "</tr><tr><th>Экипаж</th>";
    foreach($e_types as $e_type){
        if (in_array($e_type, ['Комплект эконома не верен','Комплект бизнес-класса не верен']))
            continue;
        echo "<td>" . ($k_err_num[$e_type] ?: 0) . "</td>";
    }
    echo "</tr></tbody></table>";
    ?>

    <div class="pull-right">
        <a href="/claim/?date_from=<?=$_GET['date_from']?>&date_to=<?=$_GET['date_to']?>" class="btn btn-info">Сформировать претензию</a>
    </div>
    <br />

    <!--table-striped-->
    <table class="table">
        <thead>
        <tr>
            <th class="text-center">Рейс <span class="glyphicon glyphicon-plane" aria-hidden="true"></span></th>
            <th style="width:824px;">Класс обслуживания / экипаж <span class="glyphicon glyphicon-user" aria-hidden="true"></span></th>
            <th><span class="glyphicon glyphicon-check" aria-hidden="true"></th>
        </tr>
        </thead>
        <tbody>
        <?php $i=0;?>
        <?php foreach ($flights as $flight){ ?>
            <tr>
                <td rowspan="<?=($flight['rows']+1)?>" style="vertical-align: middle; text-align: center">
                    <strong class="text-center" style="font-size: 24px;">
                        <?=$flight['number']?>
                    </strong>
                    <br />
                    <span style="font-style: italic;"><?=$flight['date_plan']?></span>
                </td>
                <td style="padding:0;border-top:none;"></td>
                <td style="padding:0;border-top:none;"></td>
            </tr>
            <?php if ($flight['econom']){ ?>
                <?php $i++;?>
                 <tr>
                     <td>
                         <a data-toggle="collapse" href="#collapseExample<?=$i?>" aria-expanded="false" aria-controls="collapseExample">
                             <strong>Эконом</strong>
                         </a>
                         <ul class="alert-danger">
                             <?php foreach ($flight['Econom']['errors'] as $error){?>
                                 <li><?=$error?></li>
                             <?php }?>
                         </ul>


                         <div class="collapse" id="collapseExample<?=$i?>">
                             <table class="table">
                                 <thead>
                                 <tr>
                                     <th>Кол-во пассаж.</th>
                                     <th>Кол-во комплектов</th>
                                     <th>Комплект по плану</th>
                                     <th>Комплект по факту</th>
                                     <th>Дельта по комплекту</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <tr>
                                     <td><?=$flight['Econom']['amount_passengers']?></td>
                                     <td><?=$flight['Econom']['amount_sets']?></td>
                                     <td><?=$flight['Econom']['set_to_be']?></td>
                                     <td><?=$flight['Econom']['set_fact']?></td>
                                     <td><?=$flight['Econom']['set_delta']?></td>
                                 </tr>
                                 </tbody>
                             </table>
                         </div>
                     </td>
                    <td style="vertical-align: middle;"><input type="checkbox" checked="checked"></td>
                 </tr>
                <?php }?>

            <?php if ($flight['Business']){?>
                <?php $i++;?>
                <tr>
                    <td>
                        <a data-toggle="collapse" href="#collapseExample<?=$i?>" aria-expanded="false" aria-controls="collapseExample">
                            <strong>Бизнес-класс</strong>
                        </a>
                        <ul class="alert-danger">
                            <?php foreach ($flight['Business']['errors'] as $error){?>
                                <li><?=$error?></li>
                            <?php }?>
                        </ul>


                        <div class="collapse" id="collapseExample<?=$i?>">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Кол-во пассаж.</th>
                                    <th>Кол-во комплектов</th>
                                    <th>Комплект по плану</th>
                                    <th>Комплект по факту</th>
                                    <th>Дельта по комплекту</th>
                                    <th>Блюда по плану</th>
                                    <th>Блюда по факту</th>
                                    <th>Дельта по блюдам</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><?=$flight['Business']['amount_passengers']?></td>
                                    <td><?=$flight['Business']['amount_sets']?></td>
                                    <td><?=$flight['Business']['set_to_be']?></td>
                                    <td><?=$flight['Business']['set_fact']?></td>
                                    <td><?=$flight['Business']['set_delta']?></td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <?php foreach ($flight['Business']['dishes_to_be'] as $dish){?>
                                                <li><?=$dish?></li>
                                            <?php }?>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <?php foreach ($flight['Business']['dishes_fact'] as $dish){?>
                                                <li><?=$dish?></li>
                                            <?php }?>
                                        </ul>
                                    </td>
                                    <td><?=$flight['Business']['dishes_delta']?></td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td style="vertical-align: middle;"><input type="checkbox" checked="checked"></td>
                </tr>
            <?php }?>
            <?php if ($flight['Crew']){?>
                <?php $i++;?>
                <tr>
                    <td>
                        <a data-toggle="collapse" href="#collapseExample<?=$i?>" aria-expanded="false" aria-controls="collapseExample">
                            <strong>Экипаж</strong>
                        </a>
                        <ul class="alert-danger">
                            <?php foreach ($flight['Crew']['errors'] as $error){?>
                                <li><?=$error?></li>
                            <?php }?>
                        </ul>


                        <div class="collapse" id="collapseExample<?=$i?>">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Кол-во пассаж.</th>
                                    <th>Кол-во комплектов</th>
                                    <th>Комплект по плану</th>
                                    <th>Комплект по факту</th>
                                    <th>Дельта по комплекту</th>
                                    <th>Блюда по плану</th>
                                    <th>Блюда по факту</th>
                                    <th>Дельта по блюдам</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><?=$flight['Crew']['amount_people']?></td>
                                    <td><?=$flight['Crew']['amount_sets']?></td>
                                    <td><?=$flight['Crew']['set_to_be']?></td>
                                    <td><?=$flight['Crew']['set_fact']?></td>
                                    <td><?=$flight['Crew']['set_delta']?></td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <?php foreach ($flight['Crew']['dishes_to_be'] as $dish){?>
                                                <li><?=$dish?></li>
                                            <?php }?>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul class="list-unstyled">
                                            <?php foreach ($flight['Crew']['dishes_fact'] as $dish){?>
                                                <li><?=$dish?></li>
                                            <?php }?>
                                        </ul>
                                    </td>
                                    <td><?=$flight['Crew']['dishes_delta']?></td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td style="vertical-align: middle;"><input type="checkbox" checked="checked"></td>
                </tr>
            <?php }?>
        <?php } ?>

        </tbody>
    </table>
</div>

<?php include('../template/footer.php')?>
